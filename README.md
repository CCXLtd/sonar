# Docker Container for Sonar


This is conainter for [sonar][1]. Its based ont eh offical [sonarqube][2] container.

## To Build

```
> docker build --tag codered/sonar . # normal build
> docker build --no-cache=true --force-rm=true --tag codered/sonar . # force a full build
```

## To Run

```
>    docker run --name sonar \
           --detach \
           --restart unless-stopped \
           --publish 8081:8081 \
           --volume /var/lib/sonar/sonatype-work:/u01/sonatype-work \
           codered/sonar
 
docker stop sonar
docker start sonar

```

[1]:  http://www.sonarqube.org/
[2]:  https://github.com/SonarSource/docker-sonarqube

